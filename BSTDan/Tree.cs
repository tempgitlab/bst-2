﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTDan
{
    class Tree
    {
        public bool Insert(int InsertValue)
        {
            if(_root == null)
            {
                _root = new Node(InsertValue);
                return true;
            }
            else
            {
                Node CurrentNode = _root;

                while (true)
                {
                    if(InsertValue == CurrentNode.Value)
                    {
                        return false;
                    }
                    else if (InsertValue < CurrentNode.Value)
                    {
                        if(CurrentNode.Left == null)
                        {
                            CurrentNode.Left = new Node(InsertValue);
                            return true;
                        }
                        CurrentNode = CurrentNode.Left;
                    }
                    else if (InsertValue > CurrentNode.Value)
                    {
                        if(CurrentNode.Right == null)
                        {
                            CurrentNode.Right = new Node(InsertValue);
                            return true;
                        }
                        CurrentNode = CurrentNode.Right;
                    }
                } 
            }
        }

        public bool Delete(int DeleteValue)
        {
            if(_root == null)
            {
                return false;
            }

            if (_root.Value == DeleteValue)
            {
                DeleteNode(ref _root);
                return true;
            }

            Node SearchNode = _root;
            
            while(true)
            {
                if (SearchNode.Left == null && SearchNode.Right == null)
                {
                    return false;
                }

                if(SearchNode.Left?.Value == DeleteValue)
                {
                    DeleteNode(ref SearchNode.Left);
                    return true;
                }
                else if (SearchNode.Right?.Value == DeleteValue)
                {
                    DeleteNode(ref SearchNode.Right);
                    return true;
                }

                if(DeleteValue < SearchNode.Value)
                {
                    SearchNode = SearchNode.Left;
                }
                else if(DeleteValue > SearchNode.Value)
                {
                    SearchNode = SearchNode.Right;
                }

            }
        }

        public int Minimum()
        {
            return FindMinimumNode(ref _root).Value;
        }

        public void PrettyPrintTree()
        {
            if (_root != null)
            {
                PrintSubtree(_root, 0);
            }      
        }

        private void PrintSubtree(Node CurrentNode, int Depth)
        {
            if(CurrentNode != null)
            {
                PrintSubtree(CurrentNode.Right, Depth + 1);
            }
            for(int i = 0; i < Depth; i++)
            {
                Console.Write("     ");
            }

            if(CurrentNode == null)
            {
                Console.WriteLine("null");
            }
            else
            {
                Console.WriteLine(CurrentNode.Value);
                PrintSubtree(CurrentNode.Left, Depth + 1);
            }

        }

        private ref Node FindMinimumNode(ref Node SearchNode)
        {
            if (SearchNode.Left == null)
            {
                return ref SearchNode;
            }
            Node PrevNode = SearchNode;
            Node CurrentNode = SearchNode.Left;
            while(CurrentNode.Left != null)
            {
                PrevNode = CurrentNode;
                CurrentNode = CurrentNode.Left; 
            }
            return ref PrevNode.Left;
        }

        private void DeleteNode(ref Node NodeRef)
        {
            if (NodeRef.Left == null && NodeRef.Right == null)
            {
                NodeRef = null;
            }
            else if (NodeRef.Left == null ^ NodeRef.Right == null)
            {
                if (NodeRef.Left != null)
                {
                    NodeRef = NodeRef.Left;
                    
                }
                else
                {
                    NodeRef = NodeRef.Right;
                    
                }
            }
            else
            {
                ref Node SmallestNode = ref FindMinimumNode(ref NodeRef.Right);
                NodeRef.Value = SmallestNode.Value;
                DeleteNode(ref SmallestNode);
            }
        }

        private Node _root;
    }
}
