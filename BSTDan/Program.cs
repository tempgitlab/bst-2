﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTDan
{
    class Program
    {
        static void Main(string[] args)
        {
            Tree MyTree = new Tree();

            List<int> MyList = new List<int> { 5,3,2,10,8,9,6,7 };
            foreach(int Element in MyList)
            {
                MyTree.Insert(Element);
            }
            MyTree.PrettyPrintTree();
            Console.WriteLine("\n\n========== DELETING 5 ==========\n\n");
            MyTree.Delete(5);
            MyTree.PrettyPrintTree();
            Console.ReadKey();
        }
    }
}
