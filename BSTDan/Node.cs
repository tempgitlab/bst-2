﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSTDan
{
    class Node
    {
        public Node(int InputValue)
        {
            Value = InputValue;
        }
        public Node() { }
        public int Value;
        public  Node Left;
        public Node Right;
    }
}
